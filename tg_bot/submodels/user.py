from django.db import models
from .bot import Bot
import json


class BotUser(models.Model):
	""" Model for bot user (Alternative for telegram chat) """

	# Relations
	bot = models.ForeignKey(Bot, on_delete=models.CASCADE)

	# Basic fields
	chat_id = models.CharField(max_length=1000)
	name = models.CharField(max_length=1000, default="-")
	phone_number = models.CharField(max_length=1000, default="-")

	# Logic fields
	status = models.CharField(max_length=1000, null=True, blank=True)
	action = models.CharField(max_length=1000, null=True, blank=True)
	json_memory = models.TextField(null=True,blank=True)

	def __str__(self):
		return f"{self.bot.name} : {self.chat_id} : {self.name}"

	def get_memory(self):
		return json.loads(self.json_memory)

	def set_memory(self, data: dict):
		try:
			self.json_memory = json.dumps(data)
			self.save()
			return {"status": "success",
					"detail": "-"}
		except Exception as e:
			return {"status": "error",
					"detail": str(e)}

	def send_text_message(self, message: str):
		response = self.bot.api.send_message(self.chat_id, message)
		return response