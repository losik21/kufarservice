from django.db import models
import telebot
from django.utils.functional import cached_property



class Bot(models.Model):

	name = models.CharField(max_length=1000, default="Unnamed")
	token = models.CharField(max_length=1000)
	host = models.CharField(max_length=1000)

	def __str__(self):
		return self.name

	###
	@cached_property
	def api(self) ->  telebot.TeleBot:
		# We must import this module here because... cross-import
		from ..logic.main import add_logic

		bot = telebot.TeleBot(self.token)
		bot = add_logic(bot)
		return bot

	@property
	def webhook_url(self):
		url = f"{self.host}:443/tg_bot/bot_webhook/{self.token}"
		return url


	def set_webhook(self):
		response = self.api.set_webhook(url=self.webhook_url)
		return response


	def remove_webhook(self):
		response = self.api.remove_webhook()
		return response


	def polling(self):
		self.api.polling(none_stop=True)


	def process_new_updates(self, json_string):
		update = telebot.types.Update.de_json(json_string)
		self.api.process_new_updates([update])

