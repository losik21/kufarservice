import telebot
from .message import MessageHandler


def add_logic(bot: telebot.TeleBot):
	""" Function appends logic to bot object and returns it than for polling or other..."""

	@bot.message_handler(func=lambda message: True)
	def text_message_handler(message):
		handler = MessageHandler(bot, message, None)
		handler.process_with_logic()

	@bot.message_handler(func=lambda message: True, content_types=['document', 'audio', 'photo'])
	def file_message_handler(message):
		handler = MessageHandler(bot, message, None)
		handler.process_with_logic()

	@bot.callback_query_handler(func=lambda call: True)
	def call_message_handler(call):
		handler = MessageHandler(bot, call.message, call)
		handler.process_with_logic()

	return bot