import telebot
import json


def logic(message_handler):
    """ Logic for Kufar Bot """

    # Not required mark. For IDE only
    from ...message import MessageHandler
    message_handler: MessageHandler
    ###

    if message_handler.message.text == "/start":
        reply_markup = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        reply_markup.add("Главное меню")
        message_handler.bot.send_message(message_handler.message.chat.id, "Kufarobot к вашим услугам!", reply_markup=reply_markup)
        return

    elif message_handler.message.text == "Главное меню":
        btn1 = telebot.types.InlineKeyboardButton("Новый запрос", callback_data='set_query')
        btn2 = telebot.types.InlineKeyboardButton("Удалить запрос", callback_data='remove_query')
        reply_markup = telebot.types.InlineKeyboardMarkup()
        reply_markup.row(btn1, btn2)
        message_handler.bot.send_message(message_handler.message.chat.id, "Меню, сэр", reply_markup=reply_markup)
        return

    elif message_handler.user_model.action == "WAIT_QUERY":
        print(message_handler.message.text)
        message_handler.user_model.action = ""
        message_handler.user_model.save()
        message_handler.bot.send_message(message_handler.message.chat.id, "Запрос установлен. Ждите оповещений. Благодарим за использование")
        return

    if message_handler.call:
        data = message_handler.call.data
        if data == "set_query":
            message_handler.user_model.action = "WAIT_QUERY"
            message_handler.user_model.save()
            message_handler.bot.delete_message(message_handler.message.chat.id, message_handler.message.message_id)
            message_handler.bot.send_message(message_handler.message.chat.id, "Введите пожалуйста ваш запрос")
            return

    message_handler.bot.send_message(message_handler.message.chat.id, "Не понятно...")
