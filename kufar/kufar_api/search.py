import requests
import json
from .product import Product
from typing import List


class URL(object):
    def __init__(self, input_string):
        self.input_string = input_string
        self.one_page_prod_count = 42

    def params(self):
        params_list: list = self.input_string.split(" ")
        return "%20".join(params_list)

    def page(self, page_number):
        query = "https://cre-api.kufar.by/ads-search/v1/engine/v1/search/rendered-paginated?query={}&ot={}&size={}&lang=ru".format(self.params(), page_number, self.one_page_prod_count)
        return query


class Search(object):
    def __init__(self, query):
        self.query = query

    @property
    def products_count(self):
        url = URL(self.query)
        page1_url = url.page(1)
        data = requests.get(page1_url)
        js = json.loads(data.content.decode())
        return js["total"]

    def products_list(self):
        url = URL(self.query)
        pages_count = self.products_count//url.one_page_prod_count + 1
        products_list = []
        for page_number in range(1, pages_count+1):
            page_url =url.page(page_number)
            response = requests.get(page_url)
            js = json.loads(response.text)
            products = js["ads"]
            for p in products:
                products_list.append(p)
        return products_list

    def products(self) -> List[Product]:
        array = []
        for product in self.products_list():
            p = Product().load_from_kufar_json(product)
            array.append(p)
        return array

# s = Search("Macbook pro 2015")
# pl = s.products()
# par = pl[0].parameters
# for p in par:
#     print(p.value_text)