from django.contrib import admin
from .models import KufarSearchRequest, KufarProduct, KufarProductParam, WebhookClient


admin.site.register(KufarSearchRequest)
admin.site.register(KufarProduct)
admin.site.register(KufarProductParam)
admin.site.register(WebhookClient)

