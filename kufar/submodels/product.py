from django.db import models
from .search import KufarSearchRequest
import json


class KufarProduct(models.Model):
	search_request = models.ForeignKey(KufarSearchRequest, on_delete=models.CASCADE)

	product_id = models.IntegerField(default=0)
	account_id = models.IntegerField(default=0)
	name = models.CharField(max_length=1000, null=True, blank=True)
	category = models.CharField(max_length=1000, null=True, blank=True)
	price_byn = models.FloatField(max_length=1000, null=True, blank=True)
	price_usd = models.FloatField(max_length=1000, null=True, blank=True)
	url = models.CharField(max_length=1000, null=True, blank=True)
	images = models.CharField(default="[]", max_length=1000)
	list_time = models.CharField( max_length=1000, null=True, blank=True)
	halva = models.BooleanField(null=True, blank=True)
	company = models.BooleanField(null=True, blank=True)
	phone = models.CharField(max_length=1000)
	currency = models.CharField(max_length=1000, default="BYR")

	old_price = models.FloatField(default=0)

	def __str__(self):
		return f"""{self.name} / {self.price_byn}"""

	@property
	def params(self):
		params = self.kufarproductparam_set.all()
		return params

	def fill_from_api_product(self, api_product):
		self.product_id = api_product.product_id
		self.account_id = api_product.account_id
		self.name = api_product.name
		self.category = api_product.category
		self.price_byn = api_product.price_byn
		self.price_usd = api_product.price_usd
		self.url = api_product.url
		self.images = json.dumps(api_product.images)
		self.list_time = api_product.list_time
		self.halva = api_product.halva
		self.company = api_product.company
		self.phone = api_product.phone
		self.currency = api_product.currency
		return self

	def get_params(self, api_params):
		model_params = []
		for param in api_params:
			model_param = self.kufarproductparam_set.create(
				product=self,
				name_ru=param.param_name_ru,
				value_text = param.value_text,
				name_en = param.param_name_en,
				value_id = param.value_id,
			)
			model_params.append(model_param)
		return model_params

	@property
	def images_list(self):
		images = []
		try:
			images = json.loads(self.images)
		except:
			pass
		return images

	@property
	def webhook_view(self):
		data = {
			"id": self.id,
			"search_query": self.search_request.query,
			"product_id": self.product_id,
			"account_id": self.account_id,
			"name": self.name,
			"category": self.category,
			"price_byn": self.price_byn,
			"price_usd": self.price_usd,
			"url": self.url,
			"halva": self.halva,
			"company": self.company,
			"images": self.images_list
		}
		return data


