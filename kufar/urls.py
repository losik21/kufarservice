from django.urls import path
from .views import create_new_search_query, remove_search_query


urlpatterns = [
	path('create_query', create_new_search_query),
	path('remove_query', remove_search_query),
]